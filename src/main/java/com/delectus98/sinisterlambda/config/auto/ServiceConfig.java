package com.delectus98.sinisterlambda.config.auto;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "com.delectus98.sinisterlambda.features.*.services",
})
public class ServiceConfig {
}
