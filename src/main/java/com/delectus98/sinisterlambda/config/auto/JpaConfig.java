package com.delectus98.sinisterlambda.config.auto;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.delectus98.sinisterlambda.features.*.repositories")
public class JpaConfig {
}
