package com.delectus98.sinisterlambda.config.auto;

import com.delectus98.sinisterlambda.config.filters.JwtAuthenticationFilter;
import com.delectus98.sinisterlambda.core.exceptions.web.ExceptionHandlerFilter;
import com.delectus98.sinisterlambda.features.user.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.*;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;
import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
@ComponentScan(basePackages = {
		"com.delectus98.sinisterlambda.config.filters",
		"com.delectus98.sinisterlambda.core.security"
})
public class WebSecurityConfig {

	@Autowired
	private ExceptionHandlerFilter exceptionHandlerFilter;

	private final JwtAuthenticationFilter jwtAuthenticationFilter;
	private final UserService userService;

	@Bean
	public SecurityFilterChain openFilterChain(HttpSecurity http) throws Exception {
		http.csrf(AbstractHttpConfigurer::disable)
			.logout(LogoutConfigurer::disable)
			.httpBasic(HttpBasicConfigurer::disable)
			.securityMatcher(antMatcher("/public/open/**"))
			.authorizeHttpRequests(request ->
					request.requestMatchers(antMatcher("/public/open/**")).permitAll()
					.anyRequest().authenticated())
			.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
			.addFilterBefore(exceptionHandlerFilter, LogoutFilter.class);
		return http.build();
	}

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.csrf(AbstractHttpConfigurer::disable)
			.logout(LogoutConfigurer::disable)
			.httpBasic(HttpBasicConfigurer::disable)
			.securityMatcher(antMatcher("/public/secured/users/**"))
			.authorizeHttpRequests(request ->
					request.requestMatchers(antMatcher("/public/secured/users/**")).authenticated())
			.sessionManagement(manager -> manager.sessionCreationPolicy(STATELESS))
			.authenticationProvider(authenticationProvider()).addFilterBefore(
					jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(exceptionHandlerFilter, LogoutFilter.class);
		return http.build();
	}

	@Bean
	public SecurityFilterChain adminFilterChain(HttpSecurity http) throws Exception {
		http.csrf(CsrfConfigurer::disable)
			.logout(LogoutConfigurer::disable)
			.httpBasic(HttpBasicConfigurer::disable)
			.securityMatcher(antMatcher("/private/secured/**"))
			.exceptionHandling(httpSecurityExceptionHandlingConfigurer -> {
				httpSecurityExceptionHandlingConfigurer.accessDeniedHandler(exceptionHandlerFilter);
			})
			.authorizeHttpRequests((requests) -> requests
					.requestMatchers(antMatcher("/private/secured/**")).hasAuthority("ADMINISTRATOR")
					.requestMatchers(antMatcher("/private/secured/actuator/**")).hasAuthority("PROMETHEUS")
					.anyRequest().authenticated()
			)
			.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
			.authenticationProvider(authenticationProvider()).addFilterBefore(
					jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
			.formLogin(FormLoginConfigurer::disable)
			.logout(LogoutConfigurer::disable)
			.addFilterBefore(exceptionHandlerFilter, LogoutFilter.class);

		return http.build();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration config)
			throws Exception {
		return config.getAuthenticationManager();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		UserDetails user =
			 User.withDefaultPasswordEncoder()
				.username("user")
				.password("password")
				.roles("USER")
				.build();

		return new InMemoryUserDetailsManager(user);
	}

}