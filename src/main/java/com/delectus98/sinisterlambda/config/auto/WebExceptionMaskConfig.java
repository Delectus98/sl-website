package com.delectus98.sinisterlambda.config.auto;

import com.delectus98.sinisterlambda.core.exceptions.web.ExceptionMappingMask;
import jakarta.validation.Valid;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;


@Configuration
public class WebExceptionMaskConfig {
    @Bean("adminMask")
    @ConfigurationProperties(prefix = "security.masks.admin")
    public ExceptionMappingMask adminMask() {
        return new ExceptionMappingMask();
    }

    @Bean("prometheusMask")
    @ConfigurationProperties(prefix = "security.masks.prometheus")
    public ExceptionMappingMask prometheusMask() {
        return new ExceptionMappingMask();
    }

//    @Bean("userMask")
//    @ConfigurationProperties(prefix = "security.masks.user")
//    public ExceptionMappingMask userMask() {
//        return new ExceptionMappingMask();
//    }
}
