package com.delectus98.sinisterlambda.config.auto;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ConfigurationPropertiesScan(basePackageClasses = {
        DatabaseConfig.class,
        JpaConfig.class,
        ServiceConfig.class,
        WebConfig.class,
        WebSecurityConfig.class,
        MapperConfig.class,
        ValidationConfig.class,
        WebExceptionMaskConfig.class
})
public class BootConfig {
}
