package com.delectus98.sinisterlambda.config.auto;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {
        "com.delectus98.sinisterlambda.core.exceptions.web",
        "com.delectus98.sinisterlambda.features.*.controllers",
})
public class WebConfig {
}
