package com.delectus98.sinisterlambda;

import com.delectus98.sinisterlambda.config.auto.BootConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = BootConfig.class)
@SpringBootApplication(scanBasePackages = {"com.delectus98.sinisterlambda"})
public class SinisterLambdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinisterLambdaApplication.class, args);
	}

}