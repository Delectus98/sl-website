package com.delectus98.sinisterlambda.core.security;

import com.delectus98.sinisterlambda.core.exceptions.security.InvalidCredentialException;
import com.delectus98.sinisterlambda.features.user.dto.JwtTokenDto;
import com.delectus98.sinisterlambda.features.user.dto.LoginPasswordDto;
import com.delectus98.sinisterlambda.features.user.dto.SignUpDto;
import com.delectus98.sinisterlambda.features.user.entities.Authority;
import com.delectus98.sinisterlambda.features.user.entities.CustomUser;
import com.delectus98.sinisterlambda.features.user.repositories.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements IAuthenticationService {
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final IJwtService jwtService;

    private final AuthenticationManager authenticationManager;

    @Transactional
    @Override
    public JwtTokenDto signUp(SignUpDto request) {
        var user = new CustomUser();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setEmail(request.getEmail());
        user.setAuthority(Authority.USER);

        userRepository.save(user);

        var jwt = jwtService.generateToken(user);
        return JwtTokenDto.builder().token(jwt).build();
    }

    @Transactional
    @Override
    public JwtTokenDto signIn(LoginPasswordDto request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));

        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new InvalidCredentialException(request.getEmail()));

        var jwt = jwtService.generateToken(user);

        return JwtTokenDto.builder().token(jwt).build();
    }
}