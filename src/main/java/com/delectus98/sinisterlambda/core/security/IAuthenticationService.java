package com.delectus98.sinisterlambda.core.security;

import com.delectus98.sinisterlambda.features.user.dto.JwtTokenDto;
import com.delectus98.sinisterlambda.features.user.dto.LoginPasswordDto;
import com.delectus98.sinisterlambda.features.user.dto.SignUpDto;

public interface IAuthenticationService {
    JwtTokenDto signUp(SignUpDto request);

    JwtTokenDto signIn(LoginPasswordDto request);

}