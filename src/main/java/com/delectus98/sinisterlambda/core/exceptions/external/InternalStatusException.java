package com.delectus98.sinisterlambda.core.exceptions.external;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class InternalStatusException extends Exception {
    private HttpStatus statusCode;

    public static class NotFoundStatusException extends InternalStatusException {

        public NotFoundStatusException() {
            super(HttpStatus.NOT_FOUND);
        }
    }
}