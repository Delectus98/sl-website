package com.delectus98.sinisterlambda.core.exceptions.security;

import org.springframework.security.core.AuthenticationException;

public class InvalidCredentialException extends AuthenticationException {
    public InvalidCredentialException(String msg) {
        super("Invalid email or password using email:" + msg);
    }
}
