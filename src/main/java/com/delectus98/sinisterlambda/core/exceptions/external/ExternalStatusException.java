package com.delectus98.sinisterlambda.core.exceptions.external;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class ExternalStatusException extends Exception {
    private HttpStatus statusCode;
}
