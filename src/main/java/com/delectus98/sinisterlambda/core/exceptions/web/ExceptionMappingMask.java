package com.delectus98.sinisterlambda.core.exceptions.web;

import com.delectus98.sinisterlambda.features.user.entities.Authority;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.validation.annotation.Validated;

import java.util.HashSet;
import java.util.Set;

/**
 * Mask to obfuscate endpoint to recipients with no special required authorities. <br/>
 * Associates a list of acceptable authorities that have sufficient requirements to see unauthorized or forbidden exception for a specific path. <br/>
 */
@Data
@Validated
public class ExceptionMappingMask {
    /**
     * List of acceptable authorities
     */
    @NotNull
    @NotEmpty
    private Set<@NotNull Authority> acceptableAuthorities;

    /**
     * Matcher of paths
     */
    @NotNull
    private AntPathRequestMatcher matcher;
}
