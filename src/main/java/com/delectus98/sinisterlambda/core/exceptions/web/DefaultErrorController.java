package com.delectus98.sinisterlambda.core.exceptions.web;

import com.delectus98.sinisterlambda.core.exceptions.external.InternalStatusException;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultErrorController implements ErrorController {

    @RequestMapping("/public/open/error")
    public void handleError() throws InternalStatusException.NotFoundStatusException {
        throw new InternalStatusException.NotFoundStatusException();
    }
}