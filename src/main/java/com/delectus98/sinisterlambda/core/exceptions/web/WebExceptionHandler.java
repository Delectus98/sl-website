package com.delectus98.sinisterlambda.core.exceptions.web;

import com.delectus98.sinisterlambda.core.exceptions.external.InternalStatusException;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.CollectionUtils;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.*;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class WebExceptionHandler {
    private final DefaultErrorAttributes errorAttributes;

    private final List<ExceptionMappingMask> masks;

    @ExceptionHandler({ UsernameNotFoundException.class, AuthenticationException.class,
            SecurityException.class,
            JwtException.class })
    public ResponseEntity<Map<String, Object>> unauthorized(WebRequest request) {
        var servletRequest = ((ServletWebRequest)request).getRequest();
        LOGGER.error("Unauthorized exception!");
        return obfuscateWithMasks(request, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<Map<String, Object>> forbidden(WebRequest request) {
        LOGGER.error("Unauthorized exception!");
        return obfuscateWithMasks(request, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(InternalStatusException.NotFoundStatusException.class)
    public ResponseEntity<Map<String, Object>> notFound(WebRequest request) {
        LOGGER.error("Not found exception!");
        return ResponseEntity.status(404).body(buildErrorAttributes(request, HttpStatus.NOT_FOUND));
    }

    @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED, reason = "Request impossible on this endpoint")
    @ExceptionHandler({ HttpRequestMethodNotSupportedException.class })
    public void notAllowed() {
        LOGGER.error("Not allowed exception!");
    }

    @ResponseStatus(value= HttpStatus.CONFLICT, reason = "Data integrity violation")  // 409
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void conflict() {
        LOGGER.error("Conflict exception!");
    }

    /**
     * Prevent endpoint with special required authorities to be seen by not appropriate recipient.  <br/>
     * Returns a 404 error status if the requirements are not sufficient.                           <br/>
     * Return {@code realStatus} if the requirements are sufficient.                                <br/>
     * @param request current request call
     * @param realStatus status return to authorized recipient otherwise status is 404 NOT FOUND.
     * @return an error status DTO with appropriate status.
     */
    private ResponseEntity<Map<String, Object>> obfuscateWithMasks(WebRequest request, HttpStatus realStatus) {
        var servletRequest = ((ServletWebRequest)request).getRequest();

        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (this.checkMatchingObfuscation(authentication, servletRequest)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(buildErrorAttributes(request, HttpStatus.NOT_FOUND));
        }

        return ResponseEntity.status(realStatus.value()).body(buildErrorAttributes(request, realStatus));
    }

    private boolean checkMatchingObfuscation(Authentication authentication, HttpServletRequest servletRequest) {
        // finding all matching endpoint that may need obfuscation
        return masks.stream().anyMatch(mask -> mask.getMatcher().matches(servletRequest)) &&
               masks.stream()
                // finding all matching endpoint that may need obfuscation
                .filter(mask -> mask.getMatcher().matches(servletRequest))
                // getting all authorities that bypass obfuscation
                .map(ExceptionMappingMask::getAcceptableAuthorities).flatMap(Collection::stream)
                // checking if no authorities corresponds to by-passer
                .noneMatch(acceptableAuth -> (
                        Optional.ofNullable(authentication).map(Authentication::getAuthorities).orElse(Collections.emptyList()).contains(acceptableAuth)));
    }

    private Map<String, Object> buildErrorAttributes(WebRequest request, HttpStatus status) {
        Map<String, Object> result = errorAttributes.getErrorAttributes(request, ErrorAttributeOptions.defaults());
        var servletRequest = ((ServletWebRequest)request).getRequest();

        result.put("status", status.value());
        result.put("error", status.getReasonPhrase());
        result.put("path", servletRequest.getRequestURI());

        return result;
    }
}
