package com.delectus98.sinisterlambda.features.user.repositories;

import com.delectus98.sinisterlambda.features.user.entities.CustomUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<CustomUser, Long> {
    @Query(value = "SELECT user FROM CustomUser user WHERE user.email = :email")
    Optional<CustomUser> findByEmail(@Param("email") String email);
}
