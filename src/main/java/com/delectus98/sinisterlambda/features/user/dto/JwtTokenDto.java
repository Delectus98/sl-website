package com.delectus98.sinisterlambda.features.user.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class JwtTokenDto implements Serializable {
    @NotNull
    @NotEmpty
    private String token;
}
