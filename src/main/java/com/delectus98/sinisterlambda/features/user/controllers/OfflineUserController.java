package com.delectus98.sinisterlambda.features.user.controllers;

import com.delectus98.sinisterlambda.core.security.IAuthenticationService;
import com.delectus98.sinisterlambda.features.user.dto.JwtTokenDto;
import com.delectus98.sinisterlambda.features.user.dto.LoginPasswordDto;
import com.delectus98.sinisterlambda.features.user.dto.SignUpDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("/public/open/users")
public class OfflineUserController {
    private final IAuthenticationService authenticationService;

    @Transactional
    @Operation(summary = "User sign in endpoint")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Account creation success", content = { @Content }),
            @ApiResponse(responseCode = "400", description = "Invalid body.", content = @Content) })
    @PostMapping(value = "/sign-up", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<JwtTokenDto> signUp(
            @Parameter(description = "New user information.") @RequestBody @Valid
            final SignUpDto signUp) {
        return ResponseEntity.ok(authenticationService.signUp(signUp));
    }

    @Transactional
    @Operation(summary = "User sign in endpoint")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Connexion attempt succeeded",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = JwtTokenDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid body.", content = @Content),
    @ApiResponse(responseCode = "401", description = "Invalid login password", content = @Content),
            @ApiResponse(responseCode = "429", description = "Too many request", content = @Content) })
    @PostMapping(value = "/sign-in", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    // make static duration of request
    public ResponseEntity<JwtTokenDto> signIn(
            @Parameter(description = "User login and password to obtain JWT authority token.") @RequestBody @Valid
            final LoginPasswordDto loginPassword) {
        return ResponseEntity.ok(authenticationService.signIn(loginPassword));
    }

}
