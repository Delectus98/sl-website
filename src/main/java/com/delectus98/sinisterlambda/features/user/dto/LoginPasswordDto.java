package com.delectus98.sinisterlambda.features.user.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class LoginPasswordDto implements Serializable {
    @Email
    private String email;

    @NotNull
    @NotBlank
    private String password;
}
