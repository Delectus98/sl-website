package com.delectus98.sinisterlambda.features.user.entities;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import org.springframework.security.core.GrantedAuthority;

public enum Authority implements GrantedAuthority {
    @JsonEnumDefaultValue
    ANONYMOUS,

    USER,

    PROMETHEUS,
    ADMINISTRATOR;

    @Override
    public String getAuthority() {
        return name();
    }
}
