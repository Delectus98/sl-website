package com.delectus98.sinisterlambda.features.supervision.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class StatusDto implements Serializable {
    private String status;
}
