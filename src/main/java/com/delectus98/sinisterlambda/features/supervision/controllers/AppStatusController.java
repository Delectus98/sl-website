package com.delectus98.sinisterlambda.features.supervision.controllers;

import com.delectus98.sinisterlambda.features.supervision.dto.StatusDto;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Validated
@Controller
@RequiredArgsConstructor
@RequestMapping("/public/open/supervision")
public class AppStatusController {

    @Operation(summary = "User sign in endpoint")
    @GetMapping("/status")
    public ResponseEntity<StatusDto> status() {
        return ResponseEntity.ok(StatusDto.builder().status("OK").build());
    }

}
