package com.delectus98.sinisterlambda;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@AutoConfigureEmbeddedDatabase
@SpringBootTest(properties = "application-test.yml")
class SinisterLambdaApplicationTests {

	@Test
	void contextLoads() {
	}

}
