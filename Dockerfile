FROM maven:3.8.3-openjdk-17

WORKDIR .
COPY . .
RUN mvn -Dmaven.test.skip clean install

CMD mvn -Dmaven.test.skip spring-boot:run

